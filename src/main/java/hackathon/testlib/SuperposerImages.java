package hackathon.testlib;

import hackathon.testlib.utils.FileUploader;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class SuperposerImages {

    public static String superPoser(String registerPath, String blackAndWhitePath) {
        BufferedImage image1 = loadImage(blackAndWhitePath);
        BufferedImage image2 = loadImage("src/main/resources/wasted.png");

        if (image1 == null || image2 == null) {
            System.out.println("Erreur lors du chargement des images.");
            throw new RuntimeException();
        }
        int x = (image1.getWidth() - image2.getWidth()) / 2;

        BufferedImage imageSuperposee = new BufferedImage(image1.getWidth(), image1.getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = imageSuperposee.createGraphics();

        g2d.drawImage(image1, 0, 0, null);

        float alpha = 0.5f; // Transparence, 0 = transparent, 1 = opaque
        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha));
        g2d.drawImage(image2, x, 0, null);

        g2d.dispose();

        String wasted_path = registerPath.split(".jpg")[0] + "_wasted.png";
        saveImage(imageSuperposee, wasted_path);

        return(wasted_path);
    }

    private static BufferedImage loadImage(String fileName) {
        try {
            return ImageIO.read(new File(fileName));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void saveImage(BufferedImage image, String fileName) {
        try {
            File wastedPic =  new File(fileName);
            ImageIO.write(image, "PNG",wastedPic);
            FileUploader.uploadImage(wastedPic);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}