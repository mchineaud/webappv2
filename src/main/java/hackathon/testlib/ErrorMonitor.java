package hackathon.testlib;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamResolution;
import hackathon.testlib.exception.GoofyException;
import hackathon.testlib.time.TimeService;
import hackathon.testlib.time.TimeServiceImpl;
import hackathon.testlib.utils.FileUploader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.imageio.ImageIO;
import java.io.*;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@ControllerAdvice
public class ErrorMonitor {
    private final TimeService timeService;
    @Autowired
    public ErrorMonitor(TimeService timeService) {
        this.timeService = timeService;
    }

    @ExceptionHandler(value = Throwable.class)
    public void PrisePhoto() throws IOException, InterruptedException {


        long elapsedTime = timeService.getElapsedTime();
        boolean firstPic = timeService.isFirstPic();
        System.out.println(elapsedTime);
        System.out.println(firstPic);

        TimeUnit.SECONDS.sleep(2);
        Webcam webcam = Webcam.getDefault();
        webcam.setViewSize(WebcamResolution.VGA.getSize());
        webcam.open();

        Analyse[] analyses = Analyse.values();
        Random random = new Random();
        int randomIndex = random.nextInt(analyses.length);
        Analyse analyseAleatoire = analyses[randomIndex];

        try {
            System.out.println(analyseAleatoire.getDescription());
            String fileName ="webcamPhoto2_" + UUID.randomUUID() + ".jpg";
            File file = new File(fileName);
            ImageIO.write(webcam.getImage(), "JPG", file);
            System.out.println("Photo captured successfully: " + file.getAbsolutePath());
            // Upload the image to the server
            FileUploader.uploadImage(file);

            String blackAndWhite_path = BlackAndWhite.transformImage(file.getAbsolutePath());
            String waster_path = SuperposerImages.superPoser(fileName, blackAndWhite_path);

            if(elapsedTime > 300000 || firstPic) {
                MessageSender.mattermostSender(analyseAleatoire, fileName);
                System.out.println("Je suis la première photo");
                timeService.setFirstPic(false);
            } else {

                MessageSender.mattermostSender(analyseAleatoire, waster_path);
                System.out.println("Et moi la seconde");
            }

            throw new GoofyException();

        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            webcam.close();
        }
    }


}
