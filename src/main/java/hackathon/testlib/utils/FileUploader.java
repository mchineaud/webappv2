package hackathon.testlib.utils;

import java.io.*;

public class FileUploader {

    public static void uploadImage(File file) throws IOException {
        // Replace the placeholder with the actual destination path on the remote server
        String destinationPath = "rocky@lbarreau.takima.me:/home/rocky/webcam-project";

        // Execute the scp command
        executeScpCommand(file.getAbsolutePath(), destinationPath);
    }


    public static void executeScpCommand(String sourcePath, String destinationPath) throws IOException {
        String command = "scp "+ sourcePath +" "+destinationPath;
        executeCommand(command);
    }

    public static void executeCommand(String command) throws IOException {
        ProcessBuilder processBuilder = new ProcessBuilder("bash", "-c", command);

        // Redirect error stream to output stream
        processBuilder.redirectErrorStream(true);

        Process process = processBuilder.start();

        // Read output
        try (InputStream inputStream = process.getInputStream();
             InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
             BufferedReader bufferedReader = new BufferedReader(inputStreamReader)) {
            System.out.println(bufferedReader.readLine());
        }

        // Wait for the process to complete
        try {
            int exitCode = process.waitFor();
            System.out.println("Command exit code: " + exitCode);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
