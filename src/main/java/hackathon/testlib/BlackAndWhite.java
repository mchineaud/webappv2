package hackathon.testlib;

import org.imgscalr.Scalr;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class BlackAndWhite {

    public static String transformImage(String path) {

        BufferedImage originalImage = loadImage(path);
        BufferedImage blackAndWhiteImage = applyBlackAndWhiteFilter(originalImage);
        String outputImagePath = path.split(".jpg")[0] + "_noir_blanc.jpg";
        saveImage(blackAndWhiteImage, outputImagePath);

        System.out.println("Filtre noir et blanc appliqué avec succès.");

        return outputImagePath;
    }

    private static BufferedImage loadImage(String imagePath) {
        try {
            return ImageIO.read(new File(imagePath));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static BufferedImage applyBlackAndWhiteFilter(BufferedImage originalImage) {
        BufferedImage blackAndWhiteImage = Scalr.apply(originalImage, Scalr.OP_GRAYSCALE);

        return blackAndWhiteImage;
    }

    private static void saveImage(BufferedImage image, String outputPath) {
        try {
            ImageIO.write(image, "jpg", new File(outputPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
