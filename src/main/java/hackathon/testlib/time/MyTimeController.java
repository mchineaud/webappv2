package hackathon.testlib.time;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyTimeController {

    private final TimeService timeService;

    @Autowired
    public MyTimeController(TimeService timeService) {
        this.timeService = timeService;
    }

    @GetMapping("/elapsed-time")
    public String getElapsedTime() {
        long elapsedTime = timeService.getElapsedTime();
        return "Elapsed time since application start: " + elapsedTime + " milliseconds";
    }
}