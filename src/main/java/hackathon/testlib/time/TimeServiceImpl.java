package hackathon.testlib.time;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TimeServiceImpl implements TimeService {

    private final Timer timer;

    @Autowired
    public TimeServiceImpl(Timer timer) {
        this.timer = timer;
    }

    @Override
    public long getElapsedTime() {
        return timer.getTimeElapsed();
    }

    @Override
    public boolean isFirstPic() {
        return timer.isFirstPic();
    }

    @Override
    public void setFirstPic(Boolean bool) {
        timer.setFirstPic(bool);
    }
}