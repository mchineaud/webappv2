package hackathon.testlib.time;

public interface TimeService {
    long getElapsedTime();
    boolean isFirstPic();
    void setFirstPic(Boolean bool);
}