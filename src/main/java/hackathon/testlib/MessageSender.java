package hackathon.testlib;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class MessageSender {

    public static void mattermostSender(Analyse analyseAleatoire, String fileName) throws IOException {

        String channelId = "z64yudwcojr73e3owuyh3nj59w";
        String message = analyseAleatoire.getDescription();
        String imageUrl = "http://lbarreau.takima.me/"+fileName;
        String token = "rw9fce98ypy43qmotge1prx9yo";
        String mattermostURL = "https://mattermost.takima.io/api/v4/posts";

        // Construire le contenu JSON de la requête
        String jsonBody = String.format("{\"channel_id\":\"%s\", \"message\":\"%s\", " +
                "\"props\":{\"attachments\":[{\"image_url\":\"%s\"}]}}", channelId, message, imageUrl);

        // Effectuer la requête POST
        URL url = new URL(mattermostURL);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Authorization", "Bearer " + token);
        connection.setDoOutput(true);

        try (OutputStream os = connection.getOutputStream()) {
            byte[] input = jsonBody.getBytes(StandardCharsets.UTF_8);
            os.write(input, 0, input.length);
        }

        // Lire la réponse
        try (BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        }

        connection.disconnect();
    }
}
